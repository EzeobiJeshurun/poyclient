import React, {Fragment, useEffect, useRef, useState } from 'react';
import styles from './styles.module.css';
import { Link } from 'react-router-dom';
import { Form, Button, Typography, Row, Col, Select, Spin, Input } from 'antd';
import PasswordInput from '../../components/PasswordInput';
import StyledInput from '../../components/StyledInput';
import {connect} from 'react-redux';
import { clearErrors, dataLoading, registerUser 
   } from '../../redux/actions/dataActions';
import { useHistory } from 'react-router-dom'; 
import  GooglePlacesAutocomplete from 'react-google-places-autocomplete'; 


const { Option } = Select;

const validateMessages = {
  required: '${name} is required!', 
  types: {
    email: '${name} must be valid!',     
    number: '${name} is not a validate number!',    
  },
  number: {
    range: '${name} must be between ${min} and ${max}',
  },
};
 
const SignUp = ({ 
  data:{ 
    currentUser, 
    networkError, 
    loading, 
    success,
    error,
    }, 
    clearErrors, 
    dataLoading,
    registerUser,
    }) => {
  const history = useHistory();    
  const removeErrors = clearErrors;
  const setLoading = dataLoading;
  const getUser = registerUser;     
  const isMounted = useRef(true);
  const [location, setLocation] = useState("");
  const [googleLocationValue, setGoogleLocationValue] = useState(null);
  const [checkLocationSet, setCheckLocationSet] = useState({});
  useEffect(() => {
    if(error.length > 0 && isMounted.current && !loading) {
      setTimeout(() => {
        removeErrors();
      }, 4000);
    };
   
    return () => {
      isMounted.current= false;
    }
  }, [isMounted, error,removeErrors,loading]);
console.log(location);
 useEffect(() => {
   

    if (!!currentUser.email) {
      history.push('/home');
  }
   return () => {
  
   }
 }, [currentUser,history]);
 
 const controlSetLocation = (value) => {
    
    setGoogleLocationValue(value);
     if(value !== null && isMounted.current){
      setLocation(value.value.description);
    }
    
 }

  const onFinish = (values) => {
    setCheckLocationSet(values);
    if(location.length > 0 && isMounted.current) {
      setLoading();
    getUser({...values, location});
    }
    
  };
    return (
        <Fragment>
            <Row className={styles.gridRow}>
              <Col className={styles.gridCol} xs={24} sm={8}>
                 <Typography className={styles.companyLabel}>Power Of You</Typography>
                 <Link  to={"/"}>
                   <Button className={styles.loginButton}  type="default">Login</Button>
                 </Link>
                <Typography  className={styles.createAccoutLabel}>Create Account</Typography>
                <Form className={styles.antForm} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                  <Form.Item
                    hasFeedback
                    name={'name'}
                  
                    rules={[
                      {
                        required: true,
                       },
                     ]}
                   >
                     <StyledInput placeholder="First Name"  />
                   </Form.Item>
                    <Form.Item
                        name={'email'}
                        hasFeedback
                        rules={[
                         {
                           type: 'email',
                           required: true,
                          },
                         ]}
                       >
                        <StyledInput placeholder="Email" />
                     </Form.Item>
                    
                   <Form.Item>
                    <GooglePlacesAutocomplete
                     selectProps={{
                       value: googleLocationValue,
                       onChange: controlSetLocation,
                       placeholder: 'Location'
                     }}
                     apiKey={process.env.REACT_APP_GOOGLE_API_KEY} />
                     <div>{!!checkLocationSet.email? (
                       !!location.length > 0?  null: <div className={styles.errorMessage}>Location is required</div>
                       ): null}</div>
                   </Form.Item>
                   
                    <Form.Item
                       name={'gender'}
                       hasFeedback
                       rules={[{ required: true, message: 'Gender is required' }]}
                       >
                         <Select className={styles.selectAnt} placeholder="Gender">
                         <Option value="male">male</Option>
                         <Option value="female">female</Option>
                         </Select>
                         </Form.Item>
                         
                        <Form.Item
                          
                          name={'password'}
                          hasFeedback
                         rules={[{ required: true, min: 8, max: 50 }]}
                        >
                         <PasswordInput placeholder="Password"/>
                      </Form.Item>
                     <Form.Item
                       name={'confirmPassword'}
                       dependencies={['password']}
                        hasFeedback
                        rules={[
                          { 
                            required: true, 
                            min: 8, 
                            max: 50,
                          },
                          ({ getFieldValue }) => ({
                               validator(rule, value) {
                                  if (!value || getFieldValue('password') === value) {
                                     return Promise.resolve();
                                  }
                             return Promise.reject('passwords must match');
                                  },
                            }),
                          ]}
                      >
                        <PasswordInput placeholder="Confirm Password" />
                      </Form.Item>
      
                     <Form.Item >
                      {loading ?
                         (<div className={styles.spinner}> <Spin /> </div>)
                       : (<Button  className={styles.signUpButton} type="primary" htmlType="submit">
                           Submit
                       </Button>)}
                       <div className={styles.errorMessage}>{error.length > 0 ? error: null}</div>
                       </Form.Item>
                    </Form>
                  </Col>
                <Col className={styles.gridCol} xs={24} sm={16}>
                 <div className={styles.gifContainer}>
                    <img src="https://www.animatedimages.org/data/media/132/animated-animal-image-0021.gif" alt="power of you" className={styles.powerGif}/>
                  </div>
                </Col>
            </Row>
        </Fragment>
);
}
 
const mapStateToProps = (state)=>({
  data: state.data,
});

const MapActionsToProp={
  clearErrors,
  dataLoading,
  registerUser,
}

export default connect(mapStateToProps, MapActionsToProp)(SignUp);