export const columns = [
        { title: "First Name", dataIndex: 'name'},
        {title: "Gender", dataIndex: 'gender'},
        { title: "Location", dataIndex: 'location'},
        {title: "Email", dataIndex: 'email'},
        { title: "Account Created", dataIndex: 'timestamp'},
    ]