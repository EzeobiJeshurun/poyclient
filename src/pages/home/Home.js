import React, { useEffect, useState, useRef } from 'react';
import styles from './styles.module.css';
import {connect} from 'react-redux';
import { clearErrors, logout } from '../../redux/actions/dataActions';
import StyledTable from '../../components/StyledTable';
import { columns } from './util';
import { Skeleton, Button } from 'antd';
import { useHistory } from 'react-router-dom';  

const Home = ({ data:{ currentUser, networkError, loading}, clearErrors, logout}) => {
    const signout = logout;
    const removeErrors = clearErrors;
    const history = useHistory();
    const isMounted = useRef(true);
    const [userDetails, setUserDetails] = useState([]);
    useEffect(() => {
        if(isMounted.current && currentUser.email) {
            setUserDetails([{
                email: currentUser.email,
                gender: currentUser.gender,
                location: currentUser.location,
                name: currentUser.name,
                timestamp: currentUser.timestamp,
                key: 1,
            }]);
        }

        if(!currentUser.email) {
            history.push('/');
            removeErrors();
        }
        return () => {
            isMounted.current = false;
        }
    }, [currentUser,history, removeErrors]);
    return (
        <div className={styles.container}>
            {userDetails.length > 0 ? <StyledTable columns={columns} dataSource={userDetails}/> : <Skeleton rows={10} active/>}
            <Button type="primary" className={styles.logout} onClick={()=>{signout()}} >Logout</Button>
        </div>
    );
}
 
 
const mapStateToProps = (state)=>({
  data: state.data,
});

const MapActionsToProp={
  clearErrors,
  logout,
}

export default connect(mapStateToProps, MapActionsToProp)(Home);

