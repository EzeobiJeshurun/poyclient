import React, { Fragment, useEffect, useRef, useState} from 'react';
import { Form, Button, Row, Col, Typography, Spin } from 'antd';
import styles from './styles.module.css';
import { Link } from 'react-router-dom';
import PasswordInput from '../../components/PasswordInput';
import StyledInput from '../../components/StyledInput';
import {connect} from 'react-redux';
import { clearErrors, dataLoading, loginUsers } from '../../redux/actions/dataActions';
import { useHistory } from 'react-router-dom'; 
 
const Signin = ({ 
  data:{ 
    currentUser, 
    networkError, 
    loading, 
    error 
    }, 
    clearErrors, 
    dataLoading, 
    loginUsers
    }) => {
  const history = useHistory();
  const removeErrors = clearErrors;
  const setLoading = dataLoading;
  const getUser = loginUsers;    
  const isMounted = useRef(true);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
  
    if(error.length > 0 && isMounted.current && !loading) {
      setTimeout(() => {
        removeErrors();
      }, 4000);
    };
    setErrorMessage(error);
 
    return () => {
      isMounted.current= false;
    }
  }, [isMounted, error,removeErrors,loading]);

  useEffect(() => {
   

    if (!!currentUser.email) {
      history.push('/home');
  }
   return () => {
  
   }
 }, [currentUser,history]);




  const onFinish = (values) => {
    setLoading();
    getUser(values);
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

    return (
        <Fragment>
            <Row className={styles.gridRow}>
            <Col className={styles.gridCol} xs={24} sm={8}>
            <Typography className={styles.companyLabel}>Power Of You</Typography>
            <Link className={styles.fullWidth} to={"/signup"}>
            <Button className={styles.createAccountButton}  type="default">Create Account</Button>
            </Link>
            <Typography className={styles.loginLabel}>Login</Typography>
            <Form 
               className={styles.antForm}
               name="basic"
               initialValues={{ remember: true }}
               onFinish={onFinish}
               onFinishFailed={onFinishFailed}
            >
              <Form.Item
                name="email"
               rules={[{ required: true, type: 'email', message: 'Please enter your email!' }]}
              >
                 <StyledInput placeholder="Email" />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
               >
                <PasswordInput  placeholder="Password"/>
             </Form.Item>

             <Form.Item >
               {loading ?
                         (<div className={styles.spinner}> <Spin /> </div>)
                       : (<Button  className={styles.loginButton} type="primary" htmlType="submit">
                           Submit
                       </Button>)}
                       <div className={styles.errorMessage}>{errorMessage}</div>
             </Form.Item>
    </Form>
            </Col>
            <Col className={styles.gridCol} xs={24} sm={16}>
              <div className={styles.gifContainer}>
                 <img src="https://www.animatedimages.org/data/media/132/animated-animal-image-0021.gif" alt="power of you" className={styles.powerGif}/>
              </div>
            </Col>
            </Row>
        </Fragment>
    );
}
 
const mapStateToProps = (state)=>({
  data: state.data,
});

const MapActionsToProp={
  clearErrors,
  dataLoading,
  loginUsers,
}

export default connect(mapStateToProps, MapActionsToProp)(Signin);
