import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import SignUp from './pages/signup';
import Signin from './pages/signin';
import Home from './pages/home';
import {Provider} from 'react-redux';
import store from './redux/store';

function App() {
  return (
    <Provider store={store} >
     <div className="App">
      <Router>
        <Switch>
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/" component={Signin} />
          <Route exact path="/home" component={Home} />
        </Switch>
      </Router>
    </div>
    </Provider>
  );
}

export default App;
