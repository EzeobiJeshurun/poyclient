import React, {memo, Fragment} from 'react';
import styles from './styles.module.css';
import { Input } from 'antd';

const PasswordInput = ({...props}) => {

    return (
        <Fragment>
        <Input.Password className={styles.inputContainer} {...props} />
        </Fragment>
    );
};
 

 
export default memo(PasswordInput);