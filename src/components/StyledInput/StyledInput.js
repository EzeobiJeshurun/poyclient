import React, {Fragment, memo } from 'react';
import { Input } from 'antd';
import styles from './styles.module.css';


const StyledInput = ({...props}) => {
    return (
        <Fragment>
        <Input className={styles.inputContainer} {...props} />
        </Fragment>
    );
}
 
export default memo(StyledInput);