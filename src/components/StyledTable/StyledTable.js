import React, { Fragment, memo } from  'react';
import { Table  } from 'antd';
 
const StyledTable = ({...props}) => {
    return (
        <Fragment>
          <Table {...props} />
        </Fragment>
    );
}
 
 
export default memo(StyledTable);