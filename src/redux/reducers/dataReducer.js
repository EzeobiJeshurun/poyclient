import { 
    REGISTER_USER, 
    ERRORS, 
    NETWORK_ERROR, 
    LOGIN,
    LOADING,
    CLEAR_ERRORS,
    LOGOUT,
    } from '../types';

const initialState = {
    error : "",
    currentUser: {},
    networkError: false,
    loading: false,
};


export default function (state= initialState, actions){
    switch(actions.type){
        case REGISTER_USER:
             return {
                 ...state,
                 currentUser: actions.payload.data,
                 loading: false,
             };
         
        case LOGIN:
        
            return {
                 ...state,
                 currentUser: actions.payload.data,
                 loading: false,
            };

        case ERRORS:
            return {
                ...state,
                error: actions.payload,
                loading: false,
            }; 
            
      
       
        case CLEAR_ERRORS:

            return {
                ...state,
                error: "",
                networkError: false,
                loading: false,
            };

        case NETWORK_ERROR:
            return {
                ...state,
                networkError: true,
                loading: false,
            };

        case LOADING: 
           return {
            ...state,
            loading: true,
          }

        case LOGOUT: 
           return {
            ...state,
            currentUser: {},
            loding: false,
            error: "",
            networkError: false,
          }          
               
        default:   
            return state;
    }
}