 import { 
    REGISTER_USER, 
    ERRORS, 
    NETWORK_ERROR, 
    LOGIN,
    LOADING,
    CLEAR_ERRORS,
    LOGOUT,
    } from '../types';

import axios from 'axios';



//remember to change all routes
export const loginUsers = (loginDetails)=>(dispatch)=>{
    axios.post('https://power-of-you.herokuapp.com/user/login/user', loginDetails)
    .then((res)=>{
        dispatch({
            type: LOGIN,
            payload: res.data
        });
    })
    .catch((err)=>{


        if(err.response) {
            //Request made and server responded
           dispatch({
             type: ERRORS,
             payload: err.response.data.error,
             });
        } else if (err.request) {
           // The request was made but no response was received
           dispatch({
             type: ERRORS,
             payload: "Something went wrong",
             });
        }else {
            //Something went wrong setting up the request
            dispatch({
             type: ERRORS,
             payload: "Please check all connections"
             });
        }
   });     

};

export const registerUser = (userDetails)=>(dispatch)=>{
    dispatch({type: LOADING});
    axios.post(`https://power-of-you.herokuapp.com/user/register/user`, userDetails)
    .then((res)=>{
        dispatch({
            type: REGISTER_USER,
            payload: res.data
        });
    })
    .catch((err)=>{
        if(err.response) {
            //Request made and server responded
           dispatch({
             type: ERRORS,
             payload: err.response.data.error,
             });
        } else if (err.request) {
           // The request was made but no response was received
           dispatch({
             type: ERRORS,
             payload: "Something went wrong",
             });
        }else {
            //Something went wrong setting up the request
            dispatch({
             type: ERRORS,
             payload: "Please check all connections"
             });
        }
       
    });

};


export const clearErrors = () =>(dispatch) => {
    dispatch({ type: CLEAR_ERRORS})
}

export const dataLoading = () =>(dispatch) => {
    dispatch({ type: LOADING });
}

export const logout = () =>(dispatch) => {
    dispatch({type: LOGOUT});
}