export const LOGIN = 'LOGIN';
export const REGISTER_USER = 'REGISTER_USER';
export const NETWORK_ERROR = 'NETWORK_ERROR';
export const ERRORS = 'ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const LOADING = ' LOADING';
export const SUCCESS = 'SUCCESS';
export const CLEAR_SUCCESS = 'CLEAR_SUCCESS';
export const LOGOUT = 'LOGOUT';

